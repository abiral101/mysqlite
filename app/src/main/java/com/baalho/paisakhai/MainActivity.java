package com.baalho.paisakhai;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DatabaseHelper myDb;
    EditText editName,editPaisa,editId;
    Button b;
    Button bv;
    Button bu;
    Button bd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDb=new DatabaseHelper(this);
         editName=(EditText) findViewById(R.id.editText);
         editPaisa=(EditText) findViewById(R.id.editText2);
         editId=(EditText) findViewById(R.id.editText3);
         b=(Button) findViewById(R.id.button);
        bv=(Button) findViewById(R.id.button2);
        bu=(Button) findViewById(R.id.button3);
        bd=(Button) findViewById(R.id.button4);
        AddData();
        viewAll();
        UpdateData();
        DeleteData();
    }
    public void DeleteData(){
        bd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer deletedRows=myDb.deleteData(editId.getText().toString());
                if(deletedRows>0){
                    Toast.makeText(MainActivity.this,"Data deleted",Toast.LENGTH_LONG).show();}
                else {
                    Toast.makeText(MainActivity.this, "Data not deleted", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public void UpdateData(){
        bu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isUpdate=myDb.updateData(editId.getText().toString(),editName.getText().toString(),editPaisa.getText().toString());
                if(isUpdate==true){
                    Toast.makeText(MainActivity.this,"Data is updated",Toast.LENGTH_LONG).show();}
                else{
                    Toast.makeText(MainActivity.this,"Data not updated",Toast.LENGTH_LONG).show();

                }
            }
        });
    }
    public void AddData(){
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isInserted = myDb.insertData(editName.getText().toString(), editPaisa.getText().toString());
                if(isInserted==true)
                    Toast.makeText(MainActivity.this,"Data is inserted",Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(MainActivity.this,"Data not inserted",Toast.LENGTH_LONG).show();
            }
        });

    }
    public void viewAll(){
        bv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Cursor res=myDb.getAllData();
                    if(res.getCount()==0){
                        //show message
                        showMessage("Error","No data found");
                        return;
                    }
                    StringBuffer buffer=new StringBuffer();
                    while(res.moveToNext()){
                        buffer.append("Id : "+res.getString(0)+"\n");
                        buffer.append("Name : "+res.getString(1)+"\n");
                        buffer.append("Paisa : "+res.getString(2)+"\n\n");

                    }
                    //show all data
                showMessage("Data",buffer.toString());
            }
        });
    }
    public void showMessage(String title, String Message){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();


    }
}
